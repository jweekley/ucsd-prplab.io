We provide images based on [Docker Stack](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html) project with NVIDIA CUDA libraries added, so that you could use those with GPUs. You can get the **list of software included** in the images on the [Docker Stack](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html) web page.

All images are available in [JupyterLab](https://jupyterhub-west.nrp-nautilus.io), and also you can refer the images directly in your [Pod definition](/userdocs/running/jupyter/).

**The list of images and registry links are available in our [GitLab registry](https://gitlab.nrp-nautilus.io/prp/jupyter-stack/container_registry)**:

* Base: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/base`
* Minimal: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/minimal`
* Desktop: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/desktop` (contains GUI desktop envorinment)
* R: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/r`
* R Studio: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/r-studio` (R studio can be launched)
* SciPy: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/scipy`
* Tensorflow: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/tensorflow`
* PRP: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/prp`
* Relion Desktop: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/relion-desktop`
* Datascience: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/datascience`
* PySpark: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/pyspark`
* All Spark: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/all-spark`

The **PRP image** (gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/prp) is based on Tensorflow with addition of:

```
    astropy
    bowtie
    fastai
    keras
    nbgitpuller
    opencv-python
    psycopg2-binary
    tensorflow-probability
    torch
    torchvision
    visualdl
    git+https://github.com/veeresht/CommPy.git
```

We can add more libraries to this image on request in [Matrix](/userdocs/start/contact/).

The Desktop image has X Window system installed and you can launch the GUI interface in Jupyter with this image. It's based on Minimal stack one.

If you're using these images in your jobs, you can [speed up the image download time by using Kraken](/userdocs/running/fast-img-download).
